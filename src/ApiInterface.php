<?php

namespace Drupal\instapage;

/**
 * Interface for Api service.
 *
 * @package Drupal\media_pixabay\Api
 */
interface ApiInterface {

  /**
   * Instapage API endpoint url.
   */
  const ENDPOINT = 'https://app.instapage.com';

  /**
   * API request method.
   */
  const METHOD = 'POST';

  /**
   * Sends out an API call and returns the results.
   *
   * @param string $action
   *   Action to execute.
   * @param array $headers
   *   Headers to send.
   * @param array $params
   *   Parameters to send.
   *
   * @return array|bool
   *   Response data or FALSE on failure.
   */
  public function createRequest($action = '', array $headers = [], array $params = []);

  /**
   * Saves a user in config and registers user through the API.
   *
   * @param string $email
   *   User email.
   * @param mixed $token
   *   Account token.
   */
  public function registerUser($email, $token);

  /**
   * Verifies the user email and password.
   *
   * @param string $email
   *   User email.
   * @param string $password
   *   User password.
   *
   * @return array
   *   Result data.
   */
  public function authenticate($email, $password);

  /**
   * Retrieves account keys for a token.
   *
   * @param mixed $token
   *   Account token.
   *
   * @return array
   *   Result data.
   */
  public function getAccountKeys($token);

  /**
   * Retrieves a list of pages for a token.
   *
   * @param string $token
   *   Account token.
   *
   * @return array|object
   *   Result data.
   */
  public function getPageList($token);

  /**
   * Returns encoded account keys.
   *
   * @param string $token
   *   Account token.
   *
   * @return bool|string
   *   Encoded account keys or FALSE on failure.
   */
  public function getEncodedKeys($token);

  /**
   * Edits an instapage page.
   *
   * @param string $page_id
   *   Id of page.
   * @param string $path
   *   Path of page.
   * @param string $token
   *   Account token.
   * @param int $publish
   *   Flag whether to publish a page.
   */
  public function editPage($page_id, $path, $token, $publish = 1);

  /**
   * Connects current domain to Drupal publishing on Instapage.
   *
   * @param string $token
   *   Account token.
   *
   * @return bool
   *   FALSE on failure, TRUE on success.
   */
  public function connectKeys($token);

  /**
   * Fetches the subaccounts from the API.
   *
   * @param string $token
   *   Account token.
   *
   * @return array
   *   Result data.
   */
  public function getSubAccounts($token);

}
