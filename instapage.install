<?php

/**
 * @file
 * Instapage.install.php.
 */

use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;

/**
 * Implements hook_install().
 *
 * Gives access to view landing pages to anonymous and authenticated users.
 */
function instapage_install() {
  // Enable default permissions for system roles.
  user_role_grant_permissions(AccountInterface::ANONYMOUS_ROLE, ['access instapage landing pages']);
  user_role_grant_permissions(AccountInterface::AUTHENTICATED_ROLE, ['access instapage landing pages']);
}

/**
 * Instapage 8.x-2.x upgrade: Clear user credentials for the new API.
 */
function instapage_update_8100() {
  $config = \Drupal::configFactory()->getEditable('instapage.settings');
  $config->set('instapage_user_id', FALSE);
  $config->set('instapage_plugin_hash', FALSE);
  $config->save();

  if (php_sapi_name() == 'cli' || (is_numeric($_SERVER['argc']) && $_SERVER['argc'] > 0)) {
    // Running from drush.
    \Drupal::messenger()->addStatus(t('Instapage has been updated. Please login with your Instapage account in the module settings.'));
  }
  else {
    $render = Markup::create('Instapage has been updated. Please login with your Instapage account in the <a href="/admin/config/services/instapage">module settings</a>.');
    \Drupal::messenger()->addStatus($render);
  }
}
