<?php

namespace Drupal\Tests\instapage\Kernel;

use Drupal\Core\Form\FormState;
use Drupal\instapage\Form\PageNewForm;
use Drupal\KernelTests\KernelTestBase;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

/**
 * Tests the PageNewForm form.
 *
 * @group instapage
 *
 * @package Drupal\Tests\instapage\Kernel
 */
class PageNewFormTest extends KernelTestBase {

  /**
   * Settings configuration variable.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $settingsConfig;

  /**
   * Pages configuration variable.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $pagesConfig;

  /**
   * Mocked Client service variable.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $mockClient;

  /**
   * Testing token variable.
   *
   * @var string
   */
  protected $token;

  /**
   * Testing email variable.
   *
   * @var string
   */
  protected $email;

  /**
   * Form builder object variable.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'instapage',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->mockClient = $this->createMock(Client::class);
    $this->mockClient
      ->method('request')
      ->will($this->onConsecutiveCalls(
        new Response(200, [], '{"success":true,"error":false,"data":{"accountkeys":["auihldailbdaibd287z738g3vb39b3z9bzigb9gf3gbfi3bf83gf8383gh38g8b3","ajsibdu28orh3obfo3bfo38fb3ozbf3izfb3zbf3zb3zbf3ib3ib3zibf3zibf33","ahsuh2o8hc3gtb3z80f38bz3b3oh8v3biz3bfu83hb3zbvhbu83bvz3bvz3bvb3b","fb3ibf389hf3bfz839hbf3bf3ufb3ubf3bf3bu3bf3bfu3bfuz3bz3bf3bf3f3bf"]},"message":"Found 4 keys"}', '1.1', 'OK'),
        new Response(200, [], '{"success":true,"error":false}', '1.1', 'OK')
      ));
    $this->container->set('http_client', $this->mockClient);
    $this->token = 'iuaphdsaihdhjsdikbfhdsjbfhskfius744758ogf83bfi3bbfbf88ob3zbfsdf3';
    $this->email = 'testing@testing.com';
    $this->settingsConfig = $this->config('instapage.settings');
    $this->pagesConfig = $this->config('instapage.pages');
    $this->formBuilder = $this->container->get('form_builder');
  }

  /**
   * Tests the buildForm() method.
   */
  public function testPageNewFormBuild() {
    $this->settingsConfig->set('instapage_user_token', $this->token);
    $this->settingsConfig->set('instapage_user_id', $this->email)->save();
    $this->pagesConfig->set('page_labels', [
      123456 => 'Testing page 1',
      234567 => 'Testing page 2',
    ]);
    $this->pagesConfig->set('instapage_pages', [
      123456 => 'testing-path-1',
      234567 => 'testing-path-2',
    ])->save();
    $form_state = new FormState();
    $form = $this->formBuilder->buildForm(PageNewForm::class, $form_state);
    $this->assertNotNull($form);
    $this->assertCount(0, $form_state->getErrors());
    $this->assertArrayHasKey('page', $form);
    $this->assertArrayHasKey('path', $form);
    $this->assertArrayHasKey('submit', $form);
    $this->assertArrayHasKey('cancel', $form);
  }

  /**
   * Tests the submitForm() method.
   */
  public function testPageNewFormSubmit() {
    $this->settingsConfig->set('instapage_user_token', $this->token);
    $this->settingsConfig->set('instapage_user_id', $this->email)->save();
    $this->pagesConfig->set('page_labels', [
      123456 => 'Testing page 1',
      234567 => 'Testing page 2',
    ]);
    $pagePaths = [123456 => 'testing-path-1'];
    $this->pagesConfig->set('instapage_pages', $pagePaths)->save();
    $form_state = (new FormState())
      ->setTriggeringElement([
        '#parents' => [
          'submit',
        ],
      ])
      ->setValues([
        'page' => '234567',
        'path' => 'testing-path-2',
      ]);
    $pagePaths[234567] = 'testing-path-2';
    $this->formBuilder->submitForm(PageNewForm::class, $form_state);
    $this->pagesConfig = $this->config('instapage.pages');
    $this->assertCount(0, $form_state->getErrors());
    $this->assertEquals($pagePaths, $this->pagesConfig->get('instapage_pages'));
  }

}
